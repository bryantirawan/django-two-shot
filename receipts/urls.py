from django.urls import path
from receipts.views import (
    AccountCreateView,
    AccountListView,
    ExpenseCategoryListView,
    ReceiptCreateView,
    ReceiptListView,
    ExpenseCategoryCreateView,
)

urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path(
        "categories/create/",
        ExpenseCategoryCreateView.as_view(),
        name="category_create",
    ),
    path(
        "create/",
        ReceiptCreateView.as_view(),
        name="receipt_create",
    ),
    path(
        "accounts/create/",
        AccountCreateView.as_view(),
        name="account_create",
    ),
    path(
        "categorylist/",
        ExpenseCategoryListView.as_view(),
        name="category_list",
    ),
    path(
        "accountslist/",
        AccountListView.as_view(),
        name="account_list",
    ),
]
