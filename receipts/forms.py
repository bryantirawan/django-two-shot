from django.forms import ModelForm
from receipts.models import Receipt, ExpenseCategory, Account


class ReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = ["vendor", "total", "tax", "date", "category", "account"]

    def __init__(self, *args, **kwargs):
        user = kwargs.pop("user")
        super(ReceiptForm, self).__init__(*args, **kwargs)
        receipts = Receipt.objects.all()
        category = receipts.values()
        print(category)
        self.fields["category"].queryset = ExpenseCategory.objects.filter(
            owner=user
        )
        self.fields["account"].queryset = Account.objects.filter(owner=user)
