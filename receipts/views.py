from calendar import c
from django.shortcuts import render, redirect
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.urls import reverse_lazy

from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.
from receipts.models import Receipt, ExpenseCategory, Account
from .forms import ReceiptForm


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/list.html"
    context_object_name = "receiptlist"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class ExpenseCategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template_name = "receipts/categorycreate.html"
    fields = ["name"]
    success_url = reverse_lazy("home")

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name = "receipts/receiptcreate.html"
    form_class = ReceiptForm

    def get_form_kwargs(self):
        kwargs = super(ReceiptCreateView, self).get_form_kwargs()
        kwargs["user"] = self.request.user
        return kwargs

    def form_valid(self, form):
        receipt = form.save(commit=False)
        receipt.purchaser = self.request.user
        receipt.save()
        form.save_m2m()
        return redirect("home")

    # this uses custom form to only show you categories and accounts from that user when creating receipt. look at forms.py for the actual custom form


class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    template_name = "receipts/accountcreate.html"
    fields = ["name", "number"]
    success_url = reverse_lazy("home")

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)


class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = "receipts/categorylist.html"
    context_object_name = "categorylist"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "receipts/accountslist.html"

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)
