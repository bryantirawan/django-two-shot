# Generated by Django 4.0.3 on 2022-05-05 19:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('receipts', '0006_alter_account_name_alter_account_number_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='receipt',
            name='account',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='receipts', to='receipts.account'),
        ),
    ]
