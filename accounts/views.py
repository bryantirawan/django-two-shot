from django.shortcuts import render
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.views import generic


# Create your views here.
# function signup(request)
#     if the request method is "POST", then
#         form = UserCreationForm(request.POST)
#         if the form is valid, then
#             username = the "username" value from the request.POST dictionary
#             password = the "password1" value from the request.POST dictionary
#             user = create a new user using username and password
#             save the user object
#             login the user with the request and user object
#             return a redirect to the "home" path
#     otherwise,
#         form = UserCreationForm(request.POST)
#     context = dictionary with key "form" and value form
#     return render with request, "registration/signup.html", and the context
class SignUpView(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy("home")
    template_name = "registration/signup.html"

    def form_valid(self, form):
        to_return = super().form_valid(form)
        login(self.request, self.object)
        return to_return
